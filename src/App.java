import erros.ListaErros;
import parser.Parser;
import scanner.Scanner;

import java.io.FileReader;

public class App {
    public static void main(String[] args) throws Exception {
        // ## Lendo a entrada pelo arquivo #
        FileReader in = new FileReader("teste01.txt");
        Scanner scanner = new Scanner(in);

        // ## Lendo a entrada pelo teclado #
        // Scanner scanner = new Scanner(System.in);
        Parser parser = new Parser(scanner);
        try {
            parser.parse();
            System.out.println("Arquivo sem erros de sintaxe!");
        } catch (Exception e) {
            System.out.println("Erro de sintaxe:" + e);
        }
    }
}